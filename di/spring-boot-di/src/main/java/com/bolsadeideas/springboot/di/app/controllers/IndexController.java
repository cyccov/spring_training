package com.bolsadeideas.springboot.di.app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.bolsadeideas.springboot.di.app.service.IHomeService;
import com.bolsadeideas.springboot.di.app.service.IndexService;
//@Controller o @Service
@Controller
public class IndexController {
	
	@Autowired
	@Qualifier("miServicioSimple") //selecciona el bean miServicioSimple para ser inyectado
	private IHomeService servicio;
	
	@Autowired
	@Qualifier("miServicioComplejo") 
	private IHomeService servicioComplejo;
	
	
//	@Autowired
//	public IndexController(IHomeService servicio) {
//	this.servicio = servicio;
//}

	@GetMapping({"/index","","/home"})
	public String index(Model model) {
		model.addAttribute("objeto", servicio.operacion()+servicioComplejo.operacion());
		return "index";
	}

//	@Autowired
//	public void setServicio(IHomeService servicio) {
//		this.servicio = servicio;
//	}
	
	
}
