package com.bolsadeideas.springboot.di.app.config;

import java.util.Arrays;
import java.util.List;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

import com.bolsadeideas.springboot.di.app.model.domain.ItemFactura;
import com.bolsadeideas.springboot.di.app.model.domain.Producto;
import com.bolsadeideas.springboot.di.app.service.ComplexService;
import com.bolsadeideas.springboot.di.app.service.HomeServiceImpl;
import com.bolsadeideas.springboot.di.app.service.IHomeService;

@Configuration
public class AppConfig {
	@Bean("miServicioSimple")
	public IHomeService registrarMiServicio() {
		return new HomeServiceImpl();
	}
	
	@Bean("miServicioComplejo")
	public IHomeService registrarMiServicioComplejo() {
		return new ComplexService();
	}
	
	@Bean("itemsFactura")
	public List<ItemFactura> registrarItems(){
		Producto producto1 = new Producto("Camara Sony", 100.50);
		Producto producto2 = new Producto("Bicicleta BIKE", 3508.56);
		ItemFactura linea = new ItemFactura(producto1, 2);
		ItemFactura linea2 = new ItemFactura(producto2, 6);
		return Arrays.asList(linea, linea2);
	}
	
	@Bean("itemsFacturaOficina")
	public List<ItemFactura> registrarItemsOficina(){
		Producto producto1 = new Producto("Monitor LG LCD 24\"", 190.50);
		Producto producto2 = new Producto("Notebook ASUS", 3508.56);
		Producto producto3 = new Producto("Impresora HP Multifuncional", 5508.56);
		Producto producto4 = new Producto("Escritorio Oficinba", 3508.56);
		ItemFactura linea = new ItemFactura(producto1, 2);
		ItemFactura linea2 = new ItemFactura(producto2, 6);
		ItemFactura linea3 = new ItemFactura(producto3, 1);
		ItemFactura linea4 = new ItemFactura(producto4, 1);
		return Arrays.asList(linea, linea2, linea3, linea4);
	}	
	
	@Bean
	public MessageSource messageSource() {
		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setBasenames("classpath:/messages/messages");//,
//				"classpath:/messages/api_response_messages");
		messageSource.setDefaultEncoding("UTF-8");
		return messageSource;
	}
}
