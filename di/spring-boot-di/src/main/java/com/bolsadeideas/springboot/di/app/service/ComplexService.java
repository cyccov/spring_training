package com.bolsadeideas.springboot.di.app.service;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

//@Component("miServicioComplejo")
//@Primary //permite seleccionar candidato a inyectar por defecto
public class ComplexService implements IHomeService{

	public String operacion() {
		return "ejecutando proceso importante complicado...";
	}
}
