package com.bolsadeideas.springboot.di.app.model.domain;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component //Singleton
//@RequestScope //Con RequestScope dura lo que dura una sesion http

public class Factura {
	@Value("${factura.descripcion}")
	private String descripcion;
	@Autowired
	private Cliente cliente;     
	@Autowired
	@Qualifier("itemsFacturaOficina")
	private List<ItemFactura> ltItems;

	@PostConstruct
	public void inicializar() {
		cliente.setNombre(cliente.getNombre().concat(" ").concat(" Yamil"));
		descripcion = descripcion.concat(" del cliente: ").concat(cliente.getNombre());
	}
	
	@PreDestroy
	public void destruir() {
		System.out.println("Factura destruida".concat(descripcion));
	}
	
	public String getDescripcion() {
		return descripcion;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public List<ItemFactura> getLtItems() {
		return ltItems;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public void setLtItems(List<ItemFactura> ltItems) {
		this.ltItems = ltItems;
	}

}
