package com.bolsadeideas.springboot.di.app.service;

import org.springframework.stereotype.Component;

//@Component("miServicioSimple")
public class HomeServiceImpl implements IHomeService{

	@Override
	public String operacion() {
		return "Operacion devuelta desde interfaz, proceso importante simple...";
	}

}
