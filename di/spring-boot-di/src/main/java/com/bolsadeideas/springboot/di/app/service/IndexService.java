package com.bolsadeideas.springboot.di.app.service;

import org.springframework.stereotype.Component;

@Component
public class IndexService {

	public String operacion() {
		return "ejecutando proceso importante...";
	}
}
