package com.bolsadeideas.springboot.app.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {
	
	@GetMapping("/")
	public String index (Model model) {
//		return "redirect:/app/index"; //para redirigir se utiliza redirect:{path} || reinicia el request || la ruta del navegador cambia
		return "forward:/app/index"; //realiza dispatch dentro del mismo request || la ruta del navegador no cambia || solo se hace con rutas internas de Spring {proyecto}
	}
}
