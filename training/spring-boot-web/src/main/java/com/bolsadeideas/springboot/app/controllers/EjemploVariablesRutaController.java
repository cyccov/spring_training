package com.bolsadeideas.springboot.app.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/variables")
public class EjemploVariablesRutaController {
	
	
	@GetMapping("/")
	public String index(Model model) {
		model.addAttribute("titulo","Enviar parámetros de la ruta @PathVariable");
		return "variables/index";
	}
	//Nota: el parametro debe de ser nombrado igual al @PathVariable
	//en caso de no hacerlo se utiliza name="parametro"
	@GetMapping("/string/{texto}")
	public String variables(@PathVariable(name = "texto") String textoOtro, Model model){
		model.addAttribute("titulos", "Recibir parametros de la ruta (@PathVariable)");
		model.addAttribute("resultado", "El texto enviado en la ruta es: "+textoOtro);
		return "variables/ver";
	}
	
	@GetMapping("/string/{texto}/{numero}")
	public String variables(
			@PathVariable(name = "texto") String textoOtro, 
			@PathVariable Integer numero, Model model){
		model.addAttribute("titulos", "Recibir parametros de la ruta (@PathVariable)");
		model.addAttribute("resultado", "El texto enviado en la ruta es: "+textoOtro+" y el numero enviado en el path es: '"+numero+"'");
		return "variables/ver";
	}
}
