package com.bolsadeideas.springboot.app.controllers;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bolsadeideas.springboot.models.Usuario;

@Controller
@RequestMapping("/app")
public class IndexController {
	
	@Value("${texto.indexcontroller.index.titulo}")
	private String textoIndex;
	@Value("${texto.perfil.index.titulo}")
	private String textoPerfil;
	@Value("${texto.listar.index.titulo}")
	private String textoListar;
	
//	@RequestMapping(value = "/index", method = RequestMethod.GET)
	@GetMapping(value = {"/index","/","/home"})
	public String index (Model model) {
		//recibe: Map<String, Object>
		//map.put("titulo","hola Srping Web");
		//ModelAndView mv
		//mv.addObject("atributo","valor");
		//mv.setViewName("index"); //se define nombre de la vista
		//retorno tipo ModelAndView
		model.addAttribute("titulo",textoIndex); //se envian datos a plantilla
		return "index"; //index.html //nombre de la vista
	}
	
	@RequestMapping("/perfil")
	public String perfil (Model model) {
		Usuario usuario = new Usuario();
		usuario.setNombre("Christian Yamil");
		usuario.setApellido("Castillo Covarrubias");
		usuario.setEmail("cyccov@gmail.com");
		model.addAttribute("titulo",textoPerfil.concat(usuario.getNombre())); //se envian datos a plantilla
		model.addAttribute("usuario", usuario);
		return "perfil";
	}
	
	@RequestMapping("/listar")
	public String listar(Model model) {
		List<Usuario> usuarios = Arrays.asList(
				new Usuario("Aleida", "Torres Sanchez", "atorress@email.com"),
				new Usuario("Dafne", "Castillo Torres", "dcastillot@email.com"),
				new Usuario("Yamil", "Castillo Covarrubias","ycastilloc@gmail.com"));
//		List<Usuario> usuarios = new ArrayList();
//		usuarios.add(new Usuario("Aleida", "Torres Sanchez", "atorress@email.com"));
//		usuarios.add(new Usuario("Dafne", "Castillo Torres", "dcastillot@email.com"));
//		usuarios.add(new Usuario("Yamil", "Castillo Covarrubias","ycastilloc@gmail.com"));
		model.addAttribute("titulo", textoListar);
		model.addAttribute("usuarios", usuarios);
		return "listar";
	}
	
	@ModelAttribute("usuarios")
	public List<Usuario> probarUsuarios(){
		List<Usuario> usuarios = Arrays.asList(
				new Usuario("Aleida", "Torres Sanchez", "atorress@email.com"),
				new Usuario("Dafne", "Castillo Torres", "dcastillot@email.com"),
				new Usuario("Yamil", "Castillo Covarrubias","ycastilloc@gmail.com"));
		return usuarios;
	}
}
